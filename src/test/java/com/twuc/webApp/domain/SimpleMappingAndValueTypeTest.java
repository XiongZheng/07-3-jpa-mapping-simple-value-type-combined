package com.twuc.webApp.domain;

import com.twuc.webApp.domain.composite.CompanyProfile;
import com.twuc.webApp.domain.composite.CompanyProfileRepository;
import com.twuc.webApp.domain.composite.UserProfile;
import com.twuc.webApp.domain.composite.UserProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleMappingAndValueTypeTest extends JpaTestBase{

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Test
    void should_save_company_profile() {

        //Arrange
        CompanyProfile companyProfile = new CompanyProfile("wuhan", "bigStreet");

        //Act
        companyProfileRepository.saveAndFlush(companyProfile);
        Optional<CompanyProfile> byId = companyProfileRepository.findById(companyProfile.getId());
        CompanyProfile companyProfile1 = byId.get();

        //Assert
        assertNotNull(companyProfile1);
        assertEquals("wuhan",companyProfile1.getCity());
        assertEquals("bigStreet",companyProfile1.getStreet());
    }

    @Test
    void should_save_user_profile() {
        UserProfile userProfile = new UserProfile("xian", "smallStreet");

        userProfileRepository.saveAndFlush(userProfile);
        Optional<UserProfile> byId = userProfileRepository.findById(userProfile.getId());
        UserProfile userProfile1 = byId.get();

        assertNotNull(userProfile1);
        assertEquals("xian",userProfile1.getAddress_city());
        assertEquals("smallStreet",userProfile.getAddress_street());
    }
}
