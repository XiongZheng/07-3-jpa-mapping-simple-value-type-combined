package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
@Table(name = "user_profile")
public class UserProfile {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false,length = 128)
    private String address_city;

    @Column(nullable = false,length = 128)
    private String address_street;

    public UserProfile() {
    }

    public UserProfile(String address_city, String address_street) {
        this.address_city = address_city;
        this.address_street = address_street;
    }

    public Long getId() {
        return id;
    }

    public String getAddress_city() {
        return address_city;
    }

    public String getAddress_street() {
        return address_street;
    }
}
